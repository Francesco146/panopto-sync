#!/usr/bin/env python

from __future__ import print_function

import time
import threading
import random
from progress_bar import CustomMixedBar


# noinspection PyUnreachableCode
if False:
    with CustomMixedBar(message="  animation") as prog:
        prog.steps_mode_init(total=10)
        inspect = lambda: time.sleep(prog.animator.tick * (prog.width + prog.shade_width) * 2)
        inspect()
        for i in range(5):
            prog.steps_hook()
            time.sleep(0.7)
        inspect()
        for i in range(5):
            prog.steps_hook()
            time.sleep(0.7)


with CustomMixedBar(message="  threads+stop") as prog:
    prog.steps_mode_init(total=40)
    def step():
        time.sleep(random.uniform(0, 0.1))
        prog.steps_hook()
    threads = [threading.Thread(target=step) for _ in range(20)]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()
